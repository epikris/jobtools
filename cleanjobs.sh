#!/bin/bash
# Note: $USER is a bash environment variabel containing your username

jobmatch=$1

numjobs=`qstat | grep $USER | grep $jobmatch | wc -l`

echo -e "Preparing to delete $numjobs jobs containing \"$jobmatch\".\nI will give you a few seconds to press ctrl+c first if you didn't mean to delete $numjobs jobs."

sleep 8

echo "Deleting jobs now"

qstat | grep $USER | grep $jobmatch | awk '{print $1}' | xargs qdel
