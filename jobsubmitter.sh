#!/bin/bash

### Settings ###
MAXJOBS=2 # Maximum simultaneous batch jobs (does not count interactive)
SLEEPTIME=60 # How long to wait before checking jobs again. Please do not set to lower than 30.
BASEDIR=$HOME # Usually you should use your home directory here
BINARY="y" # For the -b option of qsub. "y" if you're running Plink or R or similar, "n" if you're submitting a bash script

### Jobs to run ###
# This is where you set up the jobs you want to run
declare -A newjobs # Holds job name and command
declare -a joborder # Order to submit the jobs in

# If you want to create your jobs using a loop, uncomment and use the code below
#for i in `seq 1 22`
#do
#        newjobs[gwas$i]="/some/gwas/command --option whatever --file /path/to/chr$i.bgen"
#	joborder+=("gwas$i")
#done

# If you want to create your jobs manually, uncomment and use the code below
# The part in square brackets will be the job name
# The part in quotes is the job command
#      (you can also make it just part of the command, like a variable name, by modifying the $cmd variable in the loop below)
#newjobs[jobname1]="command goes here"; joborder+=("jobname1")
#newjobs[jobname2]="command goes here"; joborder+=("jobname2")
#newjobs[jobname3]="command goes here"; joborder+=("jobname3")


### Steps that make sure the code has what it needs to run ###
LOGDIR="$BASEDIR/joblogs" # Directory to output the .o and .e log files for the jobs to
WAITDIR="$BASEDIR/waitjobs" # Directory for files this script uses to track what jobs it has/hasn't launched
# Make sure these directories exist
if [ ! -d $LOGDIR ]
then
	mkdir $LOGDIR
fi
if [ ! -d $WAITDIR ]
then
	mkdir $WAITDIR
fi

while [ true ]
do
	curtime=`date +"%-I:%M:%S %p"`
	echo " "
	echo "##### $curtime #####"
	qstat | grep -v '\-\-\-' | grep -v job-ID | awk '{print $3}' | tr -d '[0-9]' | sort | uniq -c | awk '{print $2":",$1}'
	numint=`qstat | grep QLOGIN | wc -l`
	numbatch=`qstat | grep -v '\-\-\-' | grep -v job-ID | grep -v QLOGIN | wc -l`
	echo "--------"
	echo -e "Total qlogin: $numint"
	echo -e "Total batch: $numbatch"
	if [ $numbatch -lt $MAXJOBS ]
	then
		submitted=0
		for jobname in "${joborder[@]}"
		do
			if [ ! -f "$WAITDIR/$jobname" ]
			then
				submitted=1
				echo "Launching $jobname..."
				echo $curtime >> $WAITDIR/$jobname
				cmd=${newjobs[$jobname]}
				echo "Job command: $cmd"
				qsub -N $jobname -cwd -b $BINARY -o $LOGDIR -e $LOGDIR $cmd
				break
       		         fi
		done
		if [ $submitted -eq 0 ]
		then
			echo "No jobs left to submit."
		fi
	else
		echo "Currently at job limit of $MAXJOBS."
	fi
	sleep $SLEEPTIME
done
