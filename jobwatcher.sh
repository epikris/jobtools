#!/bin/bash

### Settings ###
if [ $# -eq 1 ]
then
	if [ $1 -ge 20 ]
	then
		SLEEPTIME=$1
	else
		echo "Minimum sleep time is 20 seconds. Setting sleep time to 20."
		SLEEPTIME=20
	fi
else
	SLEEPTIME=60 # How long to wait before checking jobs again. Please do not set to lower than 20.
fi

echo "Checking job status every $SLEEPTIME seconds."

while [ true ]
do
	curtime=`date +"%-I:%M:%S %p"`
	echo " "
	echo "##### $curtime #####"
	qstat | grep -v '\-\-\-' | grep -v job-ID | awk '{print $3}' | tr -d '[0-9]' | sort | uniq -c | awk '{print $2":",$1}'
	numint=`qstat | grep QLOGIN | wc -l`
	numbatch=`qstat | grep -v '\-\-\-' | grep -v job-ID | grep -v QLOGIN | wc -l`
	echo "--------"
	echo -e "Total qlogin: $numint"
	echo -e "Total batch: $numbatch"
	sleep $SLEEPTIME
done
